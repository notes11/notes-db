USE `notes_db`;

DROP TABLE IF EXISTS `note`;
CREATE TABLE `note` (
	`Id`							INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Title`							VARCHAR(255),
    `Note`							TEXT NOT NULL,
    `OwnerId`                       INT UNSIGNED NOT NULL,
    CONSTRAINT `FK_NoteId` FOREIGN KEY(`Id`) REFERENCES `base_info`(`Id`),
    CONSTRAINT `FK_OwnerId` FOREIGN KEY(`OwnerId`) REFERENCES `user`(`Id`),
    CONSTRAINT `UQ_Titel` UNIQUE(`Title`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
