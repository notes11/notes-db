USE `notes_db`;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
	`Id`						INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `UserName`					VARCHAR(50) NOT NULL,
    `Password`					VARCHAR(80) NOT NULL,
    `FirstName`					VARCHAR(50),
    `LastName`					VARCHAR(50),
    `AddressEmail`				VARCHAR(100) NOT NULL,
    `ActivationStatus`			ENUM('ACTIVE', 'INACTIVE') NOT NULL DEFAULT 'INACTIVE', 
    CONSTRAINT `FK_UserId` FOREIGN KEY(`Id`) REFERENCES `base_info`(`Id`),
    CONSTRAINT `UQ_UserName` UNIQUE(`UserName`),
    CONSTRAINT `UQ_AddressEmail` UNIQUE(`AddressEmail`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;