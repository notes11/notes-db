USE `notes_db`;

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
	`UserId`						INT UNSIGNED NOT NULL,
    `RoleId`						INT UNSIGNED NOT NULL,
    PRIMARY KEY (`UserId`, `RoleId`),
    CONSTRAINT `FK_UserRolesUserId` FOREIGN KEY(`UserId`) REFERENCES `user`(`Id`),
    CONSTRAINT `FK_UserRolesRoleId` FOREIGN KEY(`RoleId`) REFERENCES role(`Id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;