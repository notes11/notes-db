USE `notes_db`;

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
	`Id`							INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `RoleName`						VARCHAR(10) NOT NULL,
    CONSTRAINT `FK_RoleId` FOREIGN KEY(`Id`) REFERENCES `base_info`(`Id`),
    CONSTRAINT `UQ_RoleName` UNIQUE (`RoleName`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;