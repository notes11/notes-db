USE `notes_db`;

DROP TABLE IF EXISTS `base_info`;
CREATE TABLE `base_info`(
	`Id`						INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `Uuid`						VARCHAR(36) NOT NULL,
    `Version`                   INT UNSIGNED NOT NULL,
    `CreatedBy`					VARCHAR(50) NOT NULL,
    `CreatedTimestamp`			TIMESTAMP NOT NULL,
    `ModifiedBy`				VARCHAR(50),
    `ModificationTimeStamp`		TIMESTAMP,
    `EntryRelatedToTable`       VARCHAR(50) NOT NULL,
    CONSTRAINT `UQ_Uuid` UNIQUE(`Uuid`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
