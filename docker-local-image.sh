function stopDockerContainer() {
    docker stop notes-db-local
}

function buildDocekrImage() {
    docker build --pull -f ./Docker/Dockerfile -t notes-db:local .
}

function runDockerImage() {
    docker run --rm -d --name notes-db-local -p 9807:3306 -e MYSQL_ROOT_PASSWORD=notes_root_password -e MYSQL_DATABASE=notes_db -e MYSQL_USER=notes_user -e MYSQL_PASSWORD=notes_user_password notes-db:local
}

stopDockerContainer
buildDocekrImage
runDockerImage
sleep 10s