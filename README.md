# Notes-Db

Repository contains image of database `notess-db`.
Database is on MySQL image.

### Run image in container
To run image in container you can use following command:

`docker run --rm -d --name notes-db -p 9906:3306 -e MYSQL_ROOT_PASSWORD=notes_root_password -e MYSQL_DATABASE=notes_db -e MYSQL_USER=notes_user -e MYSQL_PASSWORD=notes_user_password` registry.gitlab.com/notes11/notes-db:latest

### Run image in container with `docker-compose`
Example [docker-compose.yml](Docker/docker-compose.yml) can be found here.

It can be run with command `docker-compose up -d`.